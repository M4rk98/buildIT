-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 07, 2018 at 03:53 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

--
-- Database: 'db'
--

-- --------------------------------------------------------

--
-- Table structure for table `attribute_type`
--


--
-- Dumping data for table `attribute_type`
--

INSERT INTO attribute_type (id, name) VALUES
(0, 'Manufacturer'),
(1, 'Socket'),
(2, 'Size'),
(3, 'Color'),
(4, 'CPU Type'),
(5, 'Family'),
(6, 'Capacity');

-- --------------------------------------------------------
--
-- Table structure for table `attribute_value`
--

--
-- Dumping data for table `attribute_value`
--

INSERT INTO attribute_value (id, name, attribute_type_id) VALUES
(1, 'Intel', 0),
(2, 'LGA1155', 1),
(3, '100x100', 2),
(4, '500GB', 6),
(5, 'I5', 5),
(6, '200GB', 6);

-- --------------------------------------------------------

--
-- Table structure for table `part`
--

--
-- Dumping data for table `part`
--

INSERT INTO part (id, name, price, part_category_id) VALUES
(0, 'Core i7-7700K Quad-Core', 87000, 0),
(1, 'Core i7-8700K Hexa-Core', 138190, 0),
(2, 'Core i3-8100 Quad-Core', 38990, 0),
(3, 'Core i5-7500 Quad-Core', 52500, 0),
(4, 'Core i5-7600K Quad-Core', 69990, 0),
(5, 'Core i9-7900X 10-Core', 281730, 0);

-- --------------------------------------------------------

--
-- Table structure for table `part_attribute_value`
--


--
-- Dumping data for table `part_attribute_value`
--

INSERT INTO part_attribute_value (part_id, attribute_value_id) VALUES
(0, 2),
(0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `part_category`
--


--
-- Dumping data for table `part_category`
--

INSERT INTO part_category (id, importance, name) VALUES
(0, 5, 'CPU'),
(1, 5, 'Motherboard'),
(2, 5, 'Memory'),
(3, 5, 'Storage'),
(4, 4, 'Case'),
(5, 5, 'Power supply');

-- --------------------------------------------------------

--
-- Table structure for table `part_category_attribute_type`
--

--
-- Dumping data for table `part_category_attribute_type`
--

INSERT INTO part_category_attribute_type (part_category_id, attribute_type_id) VALUES
(0, 0),
(0, 1),
(0, 5),
(0, 7),
(0, 8),
(0, 9),
(0, 10),
(0, 11);

