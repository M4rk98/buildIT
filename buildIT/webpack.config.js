var Encore = require('@symfony/webpack-encore');

Encore

    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())

    // uncomment to define the assets of the project
    // .addEntry('js/app', './assets/js/app.js')
    // .addStyleEntry('css/app', './assets/css/app.scss')

    // uncomment if you use Sass/SCSS files
    // .enableSassLoader()

    // uncomment for legacy applications that require $/jQuery as a global variable
    // .autoProvidejQuery()
    .addStyleEntry('css/main', './assets/css/main.scss')
    .addEntry('js/plugins', './assets/js/plugins.js')
    .addEntry('js/editor', './assets/js/editor.js')
    .addEntry('js/main', './assets/js/main.js')

    // allow sass/scss files to be processed
    .enableSassLoader()

    // allow legacy applications to use $/jQuery as a global variable
    .cleanupOutputBeforeBuild()
    .autoProvideVariables({ Popper: ['popper.js', 'default'] })

    .autoProvidejQuery()


;

module.exports = Encore.getWebpackConfig();
