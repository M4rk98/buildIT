<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180225154927 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_FA7AEFFB2F9A6E32');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute AS SELECT id, attribute_category_id, name FROM attribute');
        $this->addSql('DROP TABLE attribute');
        $this->addSql('CREATE TABLE attribute (id INTEGER NOT NULL, attribute_category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_FA7AEFFB2F9A6E32 FOREIGN KEY (attribute_category_id) REFERENCES attribute_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO attribute (id, attribute_category_id, name) SELECT id, attribute_category_id, name FROM __temp__attribute');
        $this->addSql('DROP TABLE __temp__attribute');
        $this->addSql('CREATE INDEX IDX_FA7AEFFB2F9A6E32 ON attribute (attribute_category_id)');
        $this->addSql('DROP INDEX IDX_490F70C68E7AEECE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__part AS SELECT id, part_category_id, name, price FROM part');
        $this->addSql('DROP TABLE part');
        $this->addSql('CREATE TABLE part (id INTEGER NOT NULL, part_category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, price INTEGER NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_490F70C68E7AEECE FOREIGN KEY (part_category_id) REFERENCES part_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO part (id, part_category_id, name, price) SELECT id, part_category_id, name, price FROM __temp__part');
        $this->addSql('DROP TABLE __temp__part');
        $this->addSql('CREATE INDEX IDX_490F70C68E7AEECE ON part (part_category_id)');
        $this->addSql('DROP INDEX IDX_3A57D1D3B6E62EFA');
        $this->addSql('DROP INDEX IDX_3A57D1D34CE34BEC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__part_attribute AS SELECT part_id, attribute_id FROM part_attribute');
        $this->addSql('DROP TABLE part_attribute');
        $this->addSql('CREATE TABLE part_attribute (part_id INTEGER NOT NULL, attribute_id INTEGER NOT NULL, PRIMARY KEY(part_id, attribute_id), CONSTRAINT FK_3A57D1D34CE34BEC FOREIGN KEY (part_id) REFERENCES part (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_3A57D1D3B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO part_attribute (part_id, attribute_id) SELECT part_id, attribute_id FROM __temp__part_attribute');
        $this->addSql('DROP TABLE __temp__part_attribute');
        $this->addSql('CREATE INDEX IDX_3A57D1D3B6E62EFA ON part_attribute (attribute_id)');
        $this->addSql('CREATE INDEX IDX_3A57D1D34CE34BEC ON part_attribute (part_id)');
        $this->addSql('DROP INDEX IDX_9ACE83318E7AEECE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_category AS SELECT id, part_category_id, name FROM attribute_category');
        $this->addSql('DROP TABLE attribute_category');
        $this->addSql('CREATE TABLE attribute_category (id INTEGER NOT NULL, part_category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_9ACE83318E7AEECE FOREIGN KEY (part_category_id) REFERENCES part_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO attribute_category (id, part_category_id, name) SELECT id, part_category_id, name FROM __temp__attribute_category');
        $this->addSql('DROP TABLE __temp__attribute_category');
        $this->addSql('CREATE INDEX IDX_9ACE83318E7AEECE ON attribute_category (part_category_id)');
        $this->addSql('DROP INDEX IDX_D48A2F7CA76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__config AS SELECT id, user_id, name FROM config');
        $this->addSql('DROP TABLE config');
        $this->addSql('CREATE TABLE config (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_D48A2F7CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO config (id, user_id, name) SELECT id, user_id, name FROM __temp__config');
        $this->addSql('DROP TABLE __temp__config');
        $this->addSql('CREATE INDEX IDX_D48A2F7CA76ED395 ON config (user_id)');
        $this->addSql('DROP INDEX IDX_D9AF2C3624DB0683');
        $this->addSql('DROP INDEX IDX_D9AF2C364CE34BEC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__config_part AS SELECT config_id, part_id FROM config_part');
        $this->addSql('DROP TABLE config_part');
        $this->addSql('CREATE TABLE config_part (config_id INTEGER NOT NULL, part_id INTEGER NOT NULL, PRIMARY KEY(config_id, part_id), CONSTRAINT FK_D9AF2C3624DB0683 FOREIGN KEY (config_id) REFERENCES config (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D9AF2C364CE34BEC FOREIGN KEY (part_id) REFERENCES part (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO config_part (config_id, part_id) SELECT config_id, part_id FROM __temp__config_part');
        $this->addSql('DROP TABLE __temp__config_part');
        $this->addSql('CREATE INDEX IDX_D9AF2C3624DB0683 ON config_part (config_id)');
        $this->addSql('CREATE INDEX IDX_D9AF2C364CE34BEC ON config_part (part_id)');
        $this->addSql('DROP INDEX IDX_472B783A4CE34BEC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__gallery AS SELECT id, part_id, name FROM gallery');
        $this->addSql('DROP TABLE gallery');
        $this->addSql('CREATE TABLE gallery (id INTEGER NOT NULL, part_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_472B783A4CE34BEC FOREIGN KEY (part_id) REFERENCES part (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO gallery (id, part_id, name) SELECT id, part_id, name FROM __temp__gallery');
        $this->addSql('DROP TABLE __temp__gallery');
        $this->addSql('CREATE INDEX IDX_472B783A4CE34BEC ON gallery (part_id)');
        $this->addSql('ALTER TABLE user ADD COLUMN roles CLOB');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_FA7AEFFB2F9A6E32');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute AS SELECT id, attribute_category_id, name FROM attribute');
        $this->addSql('DROP TABLE attribute');
        $this->addSql('CREATE TABLE attribute (id INTEGER NOT NULL, attribute_category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO attribute (id, attribute_category_id, name) SELECT id, attribute_category_id, name FROM __temp__attribute');
        $this->addSql('DROP TABLE __temp__attribute');
        $this->addSql('CREATE INDEX IDX_FA7AEFFB2F9A6E32 ON attribute (attribute_category_id)');
        $this->addSql('DROP INDEX IDX_9ACE83318E7AEECE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_category AS SELECT id, part_category_id, name FROM attribute_category');
        $this->addSql('DROP TABLE attribute_category');
        $this->addSql('CREATE TABLE attribute_category (id INTEGER NOT NULL, part_category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO attribute_category (id, part_category_id, name) SELECT id, part_category_id, name FROM __temp__attribute_category');
        $this->addSql('DROP TABLE __temp__attribute_category');
        $this->addSql('CREATE INDEX IDX_9ACE83318E7AEECE ON attribute_category (part_category_id)');
        $this->addSql('DROP INDEX IDX_D48A2F7CA76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__config AS SELECT id, user_id, name FROM config');
        $this->addSql('DROP TABLE config');
        $this->addSql('CREATE TABLE config (id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO config (id, user_id, name) SELECT id, user_id, name FROM __temp__config');
        $this->addSql('DROP TABLE __temp__config');
        $this->addSql('CREATE INDEX IDX_D48A2F7CA76ED395 ON config (user_id)');
        $this->addSql('DROP INDEX IDX_D9AF2C3624DB0683');
        $this->addSql('DROP INDEX IDX_D9AF2C364CE34BEC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__config_part AS SELECT config_id, part_id FROM config_part');
        $this->addSql('DROP TABLE config_part');
        $this->addSql('CREATE TABLE config_part (config_id INTEGER NOT NULL, part_id INTEGER NOT NULL, PRIMARY KEY(config_id, part_id))');
        $this->addSql('INSERT INTO config_part (config_id, part_id) SELECT config_id, part_id FROM __temp__config_part');
        $this->addSql('DROP TABLE __temp__config_part');
        $this->addSql('CREATE INDEX IDX_D9AF2C3624DB0683 ON config_part (config_id)');
        $this->addSql('CREATE INDEX IDX_D9AF2C364CE34BEC ON config_part (part_id)');
        $this->addSql('DROP INDEX IDX_472B783A4CE34BEC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__gallery AS SELECT id, part_id, name FROM gallery');
        $this->addSql('DROP TABLE gallery');
        $this->addSql('CREATE TABLE gallery (id INTEGER NOT NULL, part_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO gallery (id, part_id, name) SELECT id, part_id, name FROM __temp__gallery');
        $this->addSql('DROP TABLE __temp__gallery');
        $this->addSql('CREATE INDEX IDX_472B783A4CE34BEC ON gallery (part_id)');
        $this->addSql('DROP INDEX IDX_490F70C68E7AEECE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__part AS SELECT id, part_category_id, name, price FROM part');
        $this->addSql('DROP TABLE part');
        $this->addSql('CREATE TABLE part (id INTEGER NOT NULL, part_category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, price INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO part (id, part_category_id, name, price) SELECT id, part_category_id, name, price FROM __temp__part');
        $this->addSql('DROP TABLE __temp__part');
        $this->addSql('CREATE INDEX IDX_490F70C68E7AEECE ON part (part_category_id)');
        $this->addSql('DROP INDEX IDX_3A57D1D34CE34BEC');
        $this->addSql('DROP INDEX IDX_3A57D1D3B6E62EFA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__part_attribute AS SELECT part_id, attribute_id FROM part_attribute');
        $this->addSql('DROP TABLE part_attribute');
        $this->addSql('CREATE TABLE part_attribute (part_id INTEGER NOT NULL, attribute_id INTEGER NOT NULL, PRIMARY KEY(part_id, attribute_id))');
        $this->addSql('INSERT INTO part_attribute (part_id, attribute_id) SELECT part_id, attribute_id FROM __temp__part_attribute');
        $this->addSql('DROP TABLE __temp__part_attribute');
        $this->addSql('CREATE INDEX IDX_3A57D1D34CE34BEC ON part_attribute (part_id)');
        $this->addSql('CREATE INDEX IDX_3A57D1D3B6E62EFA ON part_attribute (attribute_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, username, email, password FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO user (id, username, email, password) SELECT id, username, email, password FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
    }
}
