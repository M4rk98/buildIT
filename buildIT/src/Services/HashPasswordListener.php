<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 02. 18.
 * Time: 21:23
 */
namespace App\Services;

use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HashPasswordListener implements EventSubscriber
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }


    public function prePersist(LifecycleEventArgs $eventArgs)
    {

        $entity = $eventArgs->getEntity();

        if(!$entity instanceof User)
        {
            return;
        }

        $this->encodePassword($entity);

    }

    public function getSubscribedEvents()
    {
        return ['prePersist'];
    }

    public function encodePassword(User $entity)
    {
        $encoded = $this->passwordEncoder->encodePassword($entity, $entity->getPassword());

        $entity->setPassword($encoded);
    }


}