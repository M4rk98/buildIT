<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 22.
 * Time: 11:04
 */

namespace App\Services;


class Filter
{

    private $type;
    private $filter;
    private $table;


    public function createSQLFilters($filters_decode)
    {

        $filter = new Filter();
        $filters = array();

        for($i = 0; $i < count($filters_decode); $i++)
        {

            if(isset($filters_decode[$i]['name']))
            {
                $filter->setType("name");
                $filter->setTable("av.name");
                $filter->setFilter($filters_decode[$i]['name']);

            }

            if(isset($filters_decode[$i]['id']))
            {
                $filter->setType("id");
                $filter->setTable("pc.id");
                $filter->setFilter($filters_decode[$i]['id']);
            }

            array_push($filters, $filter);


        }

        return $filters;

    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param mixed $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }




}