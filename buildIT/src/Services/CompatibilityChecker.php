<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 03. 11.
 * Time: 19:52
 */
namespace App\Services;

use App\Entity\Attribute;
use App\Entity\Part;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class CompatibilityChecker
{

    private $em;
    /**
     * @var Part[]
     */
    private $parts = [];
    public $result;


    /**
     * CompatibilityChecker constructor.
     * @param array $parts
     */
    public function __construct(EntityManagerInterface $em, $data)
    {

        foreach($data as $part_id)
        {
            $part = $em->getRepository(Part::class)->find($part_id['id']);
            array_push($this->parts, $part);
        }

        $this->em = $em;
    }


    public function check()
    {

        $is_error = false;

        foreach($this->parts as $key => $part)
        {
            foreach($part->getAttributes() as $attribute)
            {

                foreach($this->parts as $part_compare_key => $part_compare)
                {

                    if($key == $part_compare_key)
                    {
                        continue;
                    }

                    foreach($part_compare->getAttributes() as $part_compare_attribute)
                    {
                        if($attribute->getAttributeCategory()->getName() == "Foglalat" && $part_compare_attribute->getAttributeCategory()->getName() == "Foglalat")
                        {

                            if($attribute->getName() != $part_compare_attribute->getName())
                            {
                                $this->result = $attribute->getName();
                                $is_error = true;
                                return false;
                            }

                        }
                    }
                }

            }
        }

        if(!$is_error)
        {
            return true;
        }


        /*
         *
        for($i = 0; $i < count($this->attrs); $i++)
        {
            for($j = 0; $j < count($this->attrs); $j++)
            {

            }
        }
        */

        return false;
    }


    /**
     * @return array
     */
    public function getParts()
    {
        return $this->parts;
    }



}