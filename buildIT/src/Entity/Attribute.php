<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 31.
 * Time: 21:02
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="App\Repository\AttributeRepository")
 * @Doctrine\ORM\Mapping\Table(name="attribute")
 */
class Attribute
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Part", mappedBy="attributes")
     */
    private $parts;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AttributeCategory", inversedBy="attributes")
     */
    private $attributeCategory;

    /**
     * Attribute constructor.
     * @param $parts
     */
    public function __construct()
    {
        $this->parts = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAttributeCategory()
    {
        return $this->attributeCategory;
    }

    /**
     * @param mixed $attributeCategory
     */
    public function setAttributeCategory($attributeCategory)
    {
        $this->attributeCategory = $attributeCategory;
    }


}