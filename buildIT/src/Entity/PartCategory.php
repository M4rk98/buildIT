<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 31.
 * Time: 19:59
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\Table(name="part_category")
 */
class PartCategory
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Part", mappedBy="partCategory")
     */
    private $parts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AttributeCategory", mappedBy="partCategory")
     */
    private $attributeCategory;

    /**
     * PartCategory constructor.
     * @param $parts
     */
    public function __construct()
    {
        $this->parts = new ArrayCollection();
        $this->attributeCategory = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAttributeCategory()
    {
        return $this->attributeCategory;
    }

    /**
     * @param mixed $attributeCategory
     */
    public function setAttributeCategory($attributeCategory)
    {
        $this->attributeCategory = $attributeCategory;
    }


}