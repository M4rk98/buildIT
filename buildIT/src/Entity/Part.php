<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 31.
 * Time: 20:10
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="App\Repository\PartRepository")
 * @Doctrine\ORM\Mapping\Table(name="part")
 */
class Part
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="integer")
     */
    private $price;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PartCategory", inversedBy="parts")
     */
    private $partCategory;
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Attribute", inversedBy="parts")
     */
    private $attributes;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Gallery", mappedBy="part")
     */
    private $pics;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Config", mappedBy="parts")
     */
    private $config;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * Part constructor.
     * @param $attributes
     */
    public function __construct()
    {
        $this->attributes = new ArrayCollection();
        $this->pics = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return PartCategory
     */
    public function getPartCategory()
    {
        return $this->partCategory;
    }

    /**
     * @param PartCategory $partCategory
     */
    public function setPartCategory($partCategory)
    {
        $this->partCategory = $partCategory;
    }

    /**
     * @return mixed
     */
    public function getPics()
    {
        return $this->pics;
    }

    /**
     * @param mixed $pics
     */
    public function setPics($pics)
    {
        $this->pics = $pics;
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param mixed $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param mixed $expiresAt
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;
    }



}