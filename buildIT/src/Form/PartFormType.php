<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 02. 25.
 * Time: 17:33
 */
namespace App\Form;


use App\Entity\Attribute;
use App\Entity\Part;
use Proxies\__CG__\App\Entity\PartCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price')
            ->add('partCategory', EntityType::class, [
                'class' => PartCategory::class,
                'choice_label' => 'name'
            ])
            ->add('attributes', EntityType::class, [
                'class' => Attribute::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('expiresAt')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Part::class,
        ));
    }

}