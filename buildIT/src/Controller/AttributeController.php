<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 29.
 * Time: 18:34
 */

namespace App\Controller;


use App\Entity\Attribute;
use App\Entity\Part;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AttributeController extends DefaultController
{

    /**
     * @Route("/values", name="list_attribute_value_pcat")
     */
    public function getAttributeValuesByCategoryAndType()
    {
        $attributeValue = $this->getDoctrine()->getRepository(Attribute::class)->findAll();

        return $this->createApiResponse($attributeValue);
    }

    /**
     * @Route("/valueses/parts")
     */
    public function getAllPartsAndAttributesAction()
    {
        $attributeValue = $this->getDoctrine()->getRepository(Attribute::class)->findAll();


        return $this->createApiResponse($attributeValue);

    }

    /**
     * @Route("/attribute/{id}", options={"expose"=true}, name="list_attribute")
     */
    public function findAttributeById(Attribute $attribute)
    {
        return $this->createApiResponse($attribute);
    }

    /**
     * @Route("/attribute_by_part/{id}", name="list_attributeByPart")
     */
    public function findAttributesByPartAction(Part $part)
    {
        $attributes = $this->getDoctrine()->getRepository(Attribute::class)->findByPart($part);

        return $this->createApiResponse($attributes);
    }

}