<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 17.
 * Time: 14:48
 */

namespace App\Controller;


use App\Entity\Attribute;
use App\Entity\AttributeValue;
use App\Entity\Part;
use App\Entity\PartCategory;
use App\Form\PartFormType;
use App\Services\Filter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class PartController extends DefaultController
{
    /**
     * @Route("/parts", options={"expose"=true}, name="part_list")
     */
    public function getPartsFromFilters(Request $request) {

        $filters_decode = json_decode($request->getContent(), true);

        $filter = new Filter();
        $filters = $filter->createSQLFilters($filters_decode);

        $part = $this->getDoctrine()->getRepository(Part::class)->getAllPartsByFilters($filters);

        return $this->createApiResponse($part);
    }

    /**
     * @Route("/parts/{id}", options={"expose"=true}, name="part_show_js")
     */
    public function getPart(Part $part) {
        return $this->createApiResponse($part);
    }

    /**
     * @Route("/parts_by_attribute/{id}", options={"expose"=true}, name="list_parts_by_attribute")
     */
    public function findPartsByAttributeAction(Attribute $attribute)
    {
        $parts = $this->getDoctrine()->getRepository(Part::class)->findByAttr($attribute);
        return $this->createApiResponse($parts);
    }

    /**
     * @Route("/parts_by_category/{id}", options={"expose"=true}, name="list_parts_by_category")
     */
    public function findPartsByCategory(PartCategory $category)
    {
        $parts = $this->getDoctrine()->getRepository(Part::class)->findByCategory($category);
        return $this->createApiResponse($parts);

    }

    /**
     * @Route("part/new", name="part_new")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function newAction(Request $request)
    {

        $form = $this->createForm(PartFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            $this->addFlash('success', 'Sikeres hozzáadás!');

        }


        return $this->render('part/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     *@Route("part/list", name="part_list")
     *@Security("is_granted('ROLE_ADMIN')")
     */
    public function listAction()
    {

        $parts = $this->getDoctrine()->getRepository(Part::class)->findAll();

        return $this->render('part/list.html.twig', [
            'parts' => $parts
        ]);
    }

    /**
     * @Route("part/{id}/edit", name="part_show")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editAction(Part $part, Request $request)
    {

        $form = $this->createForm(PartFormType::class, $part);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $em->persist($data);
            $em->flush();

            $this->addFlash('success', 'You are AMAZING! - Part is up to date');

            return $this->redirectToRoute('part_list');
        }

        return $this->render('part/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("part/{id}/remove", name="part_remove")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function removeAction(Part $part)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($part);
        $em->flush();

        $this->addFlash('success', 'You are AMAZING! - Part is deleted!');
        return $this->redirectToRoute('part_list');

    }


}