<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 02. 18.
 * Time: 18:51
 */
namespace App\Controller;

use App\Entity\Config;
use App\Entity\Part;
use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;



class UserController extends DefaultController
{
    /**
     * @Route("/register", name="user_register")
     */
    public function registerAction(Request $request)
    {

        $form = $this->createForm(RegistrationFormType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            $this->addFlash('success', 'Szép munka! Jelentkezz be az új felhasználóddal!');

            return $this->redirectToRoute('security_login');

        }

        return $this->render('user/registration.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/user/dashboard", name="user_dashboard")
     */
    public function indexAction()
    {

        $config_repo = $this->getDoctrine()->getRepository(Config::class)->findBy([], ["uploadedAt" => "DESC"], 4);
        $expiring_parts = $this->getDoctrine()->getRepository(Part::class)->findExpiringParts();

        return $this->render('admin/dashboard.html.twig', [
            'last_configs' => $config_repo,
            'expiring_parts' => $expiring_parts
        ]);
    }

    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(AuthenticationUtils $authUtils)
    {

        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('user/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        return new \Exception("This should be not reached!");
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
        return new \Exception("This should be not reached!");
    }


    /**
     *@Route("user/list", name="user_list")
     *@Security("is_granted('ROLE_ADMIN')")
     */
    public function listAction()
    {

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('user/list.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("user/{id}/edit", name="user_show")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editAction(User $user, Request $request)
    {

        $form = $this->createForm(RegistrationFormType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $username = $user->getUsername();

            $em->persist($data);
            $em->flush();

            $this->addFlash('success', 'You are AMAZING! - '. $username .' is up to date');

            return $this->redirectToRoute('user_list');
        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("user/{id}/remove", name="user_remove")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function removeAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($user);
        $em->flush();

        $this->addFlash('success', 'You are AMAZING! - User is deleted!');
        return $this->redirectToRoute('user_list');

    }

    /**
     * @Route("/profile", name="profile")
     */
    public function updateProfileAction(UserInterface $user)
    {
        $username = $user->getUsername();

        $form = $this->createForm(RegistrationFormType::class, $user);

        return $this->render('user/edit.html.twig', [
            'username' => $username,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/user/config", options={"expose"=true}, name="user_config")
     */
    public function getConfigurations(UserInterface $user)
    {

        $configs = $this->getDoctrine()->getRepository(Config::class)->findBy(['user' => $user]);

        return $this->render('user/listConfig.html.twig', [
            'configs' => $configs
        ]);

    }


}