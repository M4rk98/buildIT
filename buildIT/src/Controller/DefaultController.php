<?php
/**
 * Created by PhpStorm.
 * User: M4rk
 * Date: 2017. 12. 02.
 * Time: 16:46
 */

namespace App\Controller;

use App\Entity\Attribute;
use App\Entity\AttributeCategory;
use App\Entity\AttributeType;
use App\Entity\AttributeValue;
use App\Entity\Gallery;
use App\Entity\Part;
use App\Entity\PartCategory;
use App\Entity\Rating;
use Doctrine\Common\Collections\ArrayCollection;
use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller
{

    protected function createApiResponse($data, $statusCode = 200)
    {

        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getName();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));


        $json = $serializer->serialize($data, 'json');

        return new JsonResponse($json, $statusCode, [], true);
    }

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render("index.html.twig");
    }

    /**
     * @Route("/upload")
     */
    public function uploadDBAction()
    {

        // read all csv
        $csv_pc = Reader::createFromPath('../data/part_category.csv');
        $csv_ac = Reader::createFromPath('../data/attributeCategory.csv');
        $csv_p = Reader::createFromPath('../data/part.csv');
        $csv_a = Reader::createFromPath('../data/attribute.csv');
        $csv_pa = Reader::createFromPath('../data/part_attribute_value.csv');
        $csv_g = Reader::createFromPath('../data/gallery.csv');

        $em = $this->getDoctrine()->getManager();

        // Part category
        $csv_pc->setDelimiter(';');
        $csv_pc->setHeaderOffset(0);

        foreach($csv_pc as $result)
        {
            $part_category = new PartCategory();
            $part_category->setName($result['name']);

            //$rate = $this->getDoctrine()->getRepository(Rating::class)->find($result['importance']);

            //$part_category->setImportance($rate);

            $em->persist($part_category);
            $em->flush();
        }


        // Attribute category
        $csv_ac->setDelimiter(';');
        $csv_ac->setHeaderOffset(0);

        foreach($csv_ac as $result)
        {
            $attribute_category = new AttributeCategory();
            $attribute_category->setName($result['name']);

            $part_category = $this->getDoctrine()->getRepository(PartCategory::class)->find($result['part_category_id']);

            $attribute_category->setPartCategory($part_category);

            $em->persist($attribute_category);
            $em->flush();
        }


        // Part
        $csv_p->setDelimiter(';');
        $csv_p->setHeaderOffset(0);

        foreach($csv_p as $result)
        {
            $part = new Part();
            $part->setName($result['name']);
            $part->setPrice($result['price']);

            $part_category = $this->getDoctrine()->getRepository(PartCategory::class)->find($result['part_category_id']);

            $part->setPartCategory($part_category);

            $em->persist($part);
            $em->flush();
        }


        // Attribute
        $csv_a->setDelimiter(';');
        $csv_a->setHeaderOffset(0);

        foreach($csv_a as $result)
        {
            $attribute = new Attribute();
            $attribute->setName($result['name']);

            $attribute_category = $this->getDoctrine()->getRepository(AttributeCategory::class)->find($result['attribute_category_id']);

            $attribute->setAttributeCategory($attribute_category);

            $em->persist($attribute);
            $em->flush();
        }


        // csv_pa
        $csv_pa->setHeaderOffset(0);
        $csv_pa->setDelimiter(';');

        foreach($csv_pa as $result) {

            $attributes = $result['attribute_id'];
            $attributes_parts = explode(',', $attributes);
            $attrs = [];

            $part = $this->getDoctrine()->getRepository(Part::class)->find($result['part_id']);

            foreach ($attributes_parts as $attribute_id) {
                $attribute = $this->getDoctrine()->getRepository(Attribute::class)->find($attribute_id);
                $attrs[] = $attribute;
            }

            $part->setAttributes($attrs);
            $em->persist($part);
            $em->flush();

        }

        $csv_g->setHeaderOffset(0);
        $csv_g->setDelimiter(';');

        foreach($csv_g as $result)
        {
            $gallery = new Gallery();
            $gallery->setName($result['name']);

            $part = $this->getDoctrine()->getRepository(Part::class)->find($result['part_id']);

            $gallery->setPart($part);

            $em->persist($gallery);
            $em->flush();

        }

        /*
        $csv_p->setHeaderOffset(0);
        $csv_p->setDelimiter(';');

        foreach($csv_p as $result)
        {
            $part = new Part();
            $part->setName($result['name']);
            $part->setPrice($result['price']);

            $part_category = $this->getDoctrine()->getRepository(PartCategory::class)->find($result['part_category_id']);

            $part->setPartCategory($part_category);

            //$part->setAttributes();

            $em->persist($part);
            $em->flush();

        }

        // Attribute type upload
        $csv_at->setHeaderOffset(0);
        $em = $this->getDoctrine()->getManager();

        foreach($csv_at as $result)
        {
            $attr_type = new AttributeCategory();
            $attr_type->setName($result['name']);

            $em->persist($attr_type);
            $em->flush();
        }

        // Attribute value upload
        $csv_av->setHeaderOffset(0);
        $csv_av->setDelimiter(';');

        foreach($csv_av as $result)
        {

            $attr_value = new Attribute();
            $attr_value->setName($result["name"]);

            $attribute_category = $this->getDoctrine()->getRepository(AttributeCategory::class)->find($result["attribute_category_id"]);
            $attr_value->setAttributeCategory($attribute_category);

            $em->persist($attr_value);
            $em->flush();

        }


        // part attribute_value
        $csv_p_av->setHeaderOffset(0);
        $csv_p_av->setDelimiter(';');

        foreach($csv_p_av as $result)
        {

            $parts_id = $result['part_id'];
            $parts_parts = explode(',', $parts_id);

            $attribute = $this->getDoctrine()->getRepository(Attribute::class)->find($result['attribute_id']);
            $parts = [];

            foreach($parts_parts as $part_id)
            {
                if($part_id == "")
                {
                    continue;
                }
                $part = $this->getDoctrine()->getRepository(Part::class)->find($part_id);
                $parts[] = $part;
            }

            $attribute->setParts($parts);
            $em->persist($attribute);
            $em->flush();

        }

        $csv_av_pc->setHeaderOffset(0);
        $csv_av_pc->setDelimiter(';');

        foreach($csv_av_pc as $result)
        {

            $part_categories = $result['part_category_id'];
            $part_categories = explode(',', $part_categories);

            $part_categories_a =  [];

            foreach($part_categories as $part_category_id)
            {

                $part_category = $this->getDoctrine()->getRepository(PartCategory::class)->find($part_category_id);
                $part_categories_a[] = $part_category;


            }

            $attr_value = $this->getDoctrine()->getRepository(Attribute::class)->find($result['attribute_id']);
            $attr_value->setPartCategory($part_categories_a);
            $em->persist($attr_value);
            $em->flush();


        }

        // part_category Attribute type
        $csv_pc_at->setHeaderOffset(0);
        $csv_pc_at->setDelimiter(';');

        foreach($csv_pc_at as $result)
        {

            $part_categories = $result['part_category_id'];
            $part_categories_parts = explode(',', $part_categories);
            $part_cs = [];

            foreach($part_categories_parts as $part_category_id)
            {

                $part_category = $this->getDoctrine()->getRepository(PartCategory::class)->find($part_category_id);
                $part_cs[] = $part_category;
            }


            $attributeCategory = $this->getDoctrine()->getRepository(AttributeCategory::class)->find($result['attribute_category_id']);
            $attributeCategory->setPartCategory($part_cs);
            $em->persist($attributeCategory);
            $em->flush();

        }

        //gallery
        $csv_g->setHeaderOffset(0);
        $csv_g->setDelimiter(';');

        foreach($csv_g as $result)
        {
            $gallery = new Gallery();
            $gallery->setName($result['name']);

            $part = $this->getDoctrine()->getRepository(Part::class)->find($result['part_id']);

            $gallery->setPart($part);

            $em->persist($gallery);
            $em->flush();

        }

        /*
        // Attribute type upload
        $csv_at->setHeaderOffset(0);
        $em = $this->getDoctrine()->getManager();

        foreach($csv_at as $result)
        {
            $attr_type = new AttributeType();
            $attr_type->setName($result['name']);

            $em->persist($attr_type);
            $em->flush();
        }

        /*

        // Attribute value upload
        $csv_av->setHeaderOffset(0);
        $csv_av->setDelimiter(';');

        foreach($csv_av as $result)
        {

            $attr_value = new AttributeValue();
            $attr_value->setName($result["name"]);

            $attr_type = $this->getDoctrine()->getRepository(AttributeType::class)->find($result["attribute_type_id"]);

            $attr_value->setAttributeType($attr_type);

            $em->persist($attr_value);
            $em->flush();

        }

        // Rating
        $csv_r->setHeaderOffset(0);

        foreach($csv_r as $result)
        {
            $rating = new Rating();
            $rating->setRating($result['rating']);

            $em->persist($rating);
            $em->flush();

        }


        // Part category
        $csv_pc->setDelimiter(';');
        $csv_pc->setHeaderOffset(0);

        foreach($csv_pc as $result)
        {
            $part_category = new PartCategory();
            $part_category->setName($result['name']);

            $rate = $this->getDoctrine()->getRepository(Rating::class)->find($result['importance']);

            $part_category->setImportance($rate);

            $em->persist($part_category);
            $em->flush();
        }



        // Part
        $csv_p->setHeaderOffset(0);
        $csv_p->setDelimiter(';');

        foreach($csv_p as $result)
        {
            $part = new Part();
            $part->setName($result['name']);
            $part->setPrice($result['price']);

            var_dump($result);

            $part_category = $this->getDoctrine()->getRepository(PartCategory::class)->find($result['part_category_id']);

            $part->setPartCategory($part_category);

            //$part->setAttributes();

            $em->persist($part);
            $em->flush();

        }



        // part_category Attribute type
        $csv_pc_at->setHeaderOffset(0);
        $csv_pc_at->setDelimiter(';');

        foreach($csv_pc_at as $result)
        {

            $attributes = $result['attribute_type_id'];
            $attributes_parts = explode(',', $attributes);
            $attr_types = [];

            foreach($attributes_parts as $attribute_type_id)
            {

                $attr_type = $this->getDoctrine()->getRepository(AttributeType::class)->find($attribute_type_id);
                $attr_types[] = $attr_type;
            }


            $part_category = $this->getDoctrine()->getRepository(PartCategory::class)->find($result['part_category_id']);
            $part_category->setAttributes($attr_types);
            $em->persist($part_category);
            $em->flush();

        }


        // part attribute_value
        $csv_p_av->setHeaderOffset(0);
        $csv_p_av->setDelimiter(';');

        foreach($csv_p_av as $result)
        {

            $attributes = $result['attribute_value_id'];
            $attributes_parts = explode(',', $attributes);
            $attr_values = [];

            foreach($attributes_parts as $attribute_value_id)
            {

                $attr_value = $this->getDoctrine()->getRepository(AttributeValue::class)->find($attribute_value_id);
                $attr_values[] = $attr_value;
            }


            $part = $this->getDoctrine()->getRepository(Part::class)->find($result['part_id']);
            $part->setAttributes($attr_values);
            $em->persist($part);
            $em->flush();

        }

        // part attribute_value
        $csv_pc_av->setHeaderOffset(0);
        $csv_pc_av->setDelimiter(';');

        foreach($csv_pc_av as $result)
        {

            $attributes = $result['attribute_value_id'];
            $attributes_parts = explode(',', $attributes);
            $attr_values = [];

            foreach($attributes_parts as $attribute_value_id)
            {
                $attr_value = $this->getDoctrine()->getRepository(AttributeValue::class)->find($attribute_value_id);
                $attr_values[] = $attr_value;
            }

            $partCategory = $this->getDoctrine()->getRepository(PartCategory::class)->find($result['part_category_id']);
            $partCategory->setAttrs($attr_values);
            $em->persist($partCategory);
            $em->flush();

            /*s = [];

            $part_category = new ArrayCollection();
            $part_category[] = $this->getDoctrine()->getRepository(PartCategory::class)->find($result['part_category_id']);

            foreach($attributes_parts as $attribute_value_id)
            {

                $attr_value = $this->getDoctrine()->getRepository(AttributeValue::class)->find($attribute_value_id);
                $attr_values[] = $attr_value;
                $attr_value->setPartCategory($part_category);
                $em->persist($attr_value);
                $em->flush();





        }


        //gallery
        $csv_g->setHeaderOffset(0);
        $csv_g->setDelimiter(';');

        foreach($csv_g as $result)
        {
            $gallery = new Gallery();
            $gallery->setName($result['name']);

            $part = $this->getDoctrine()->getRepository(Part::class)->find($result['part_id']);

            $gallery->setPart($part);

            $em->persist($gallery);
            $em->flush();

        }
        */

        return new Response('<h1>ok</h1>');
    }

}