<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 16.
 * Time: 12:53
 */

namespace App\Controller;

use App\Entity\PartCategory;
use Symfony\Component\Routing\Annotation\Route;


class PartCategoryController extends DefaultController
{
    /**
     * @Route("/category/{id}", options={"expose"=true}, name="list_category")
     */
    public function findCategoryById(PartCategory $partCategory)
    {

        return $this->createApiResponse($partCategory);
    }
}