<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 02. 08.
 * Time: 17:18
 */

namespace App\Controller;

use App\Entity\Part;
use App\Entity\PartCategory;
use App\Services\CompatibilityChecker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Config;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("is_granted(['ROLE_USER', 'ROLE_ADMIN'])")
 */
class ConfigController extends DefaultController
{
    /**
     * @Route("/config/new", options={"expose"=true}, name="new_config")
     */
    public function addConfigAction(Request $request, UserInterface $user)
    {
        $data = json_decode($request->getContent(), true);

        $parts = [];
        $em = $this->getDoctrine()->getManager();

        $name = array_shift($data)['name'];

        $compatibilityChecker = new CompatibilityChecker($em, $data);
        if(!$compatibilityChecker->check())
        {
            return $this->createApiResponse($compatibilityChecker->result, 300);
        }

        foreach($data as $part_id)
        {
            $part = $this->getDoctrine()->getRepository(Part::class)->find($part_id['id']);
            array_push($parts, $part);
        }

        // Megnézzük, hogy a konfig amit hozzáadtak létezik-e már
        $config = $this->getDoctrine()->getRepository(Config::class)->findOneBy(["name" => $name]);

        if(empty($config)) {
            $config = new Config();
        }

        $config->setName($name);
        $config->setParts($parts);
        $config->setUser($user);
        $datetime = new \DateTime('now');
        $config->setUploadedAt($datetime);

        $em->persist($config);
        $em->flush();


        return $this->createApiResponse($data);


    }

    /**
     * @Route("/config/{id}/edit", name="config_edit")
     */
    public function editAction(Config $config)
    {

        $em = $this->getDoctrine();

        $part_repo = $em->getRepository(Part::class);
        $category_repo = $em->getRepository(PartCategory::class);
        $config_repo = $em->getRepository(Config::class);

        $current_parts = $config->getParts();

        $categories = $category_repo->findAll();
        $parts = $part_repo->findBy([], null, 12);
        $configs = $config_repo->findAll();

        return $this->render("editor/list.html.twig", [
            'parts' => $parts,
            'categories' => $categories,
            'configs' => $configs,
            'current_parts' => $current_parts
        ]);

    }


}