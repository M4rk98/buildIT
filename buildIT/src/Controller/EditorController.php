<?php
/**
 * Created by PhpStorm.
 * User: M4rk
 * Date: 2018. 01. 02.
 * Time: 15:02
 */

namespace App\Controller;

use App\Entity\Config;
use App\Entity\Part;
use App\Entity\PartCategory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("is_granted(['ROLE_USER', 'ROLE_ADMIN'])")
 */
class EditorController extends DefaultController
{

    /**
     * @Route("/editor", name="editor_homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $part_repo = $em->getRepository(Part::class);
        $category_repo = $em->getRepository(PartCategory::class);
        $config_repo = $em->getRepository(Config::class);

        $current_parts = $part_repo->find(2);

        $categories = $category_repo->findAll();
        $parts = $part_repo->findBy([], null, 12);
        $configs = $config_repo->findAll();

        return $this->render("editor/list.html.twig", [
            'parts' => $parts,
            'categories' => $categories,
            'configs' => $configs,
            'current_parts' => $current_parts
        ]);
    }

}