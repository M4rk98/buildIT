<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 29.
 * Time: 19:14
 */

namespace App\Repository;


use App\Entity\AttributeValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\PartCategory;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AttributeValueRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AttributeValue::class);
    }


    public function findByPartCategoryAttrValue($partCategory)
    {
        return $this->createQueryBuilder('av')
            ->join('av.partCategory', 'pc')
            ->andWhere('pc.name = :attrs')
            ->setParameter('attrs', 'Processzor')
            ->select('av.name')
            ->getQuery()
            ->getResult();
    }

}