<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 08.
 * Time: 0:20
 */

namespace App\Repository;


use App\Entity\PartCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PartCategoryRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PartCategory::class);
    }


    public function findOne($id)
    {
        return $this->createQueryBuilder('pc')
            ->join('pc.attributes', 'at')
            ->join('pc.attrs', 'av')
            ->join('pc.parts', 'p')
            ->andWhere('pc.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function getPartCategoryAttributesById($id)
    {

    }

}