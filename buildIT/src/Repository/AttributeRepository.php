<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 02. 04.
 * Time: 12:06
 */

namespace App\Repository;


use App\Entity\Attribute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AttributeRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Attribute::class);
    }

    public function findByPart($part)
    {
        return $this->createQueryBuilder("attribute")
            ->join('attribute.parts', 'p')
            ->andWhere('p.id = :part')
            ->setParameter('part', $part->getId())
            ->getQuery()
            ->getResult();
    }

}