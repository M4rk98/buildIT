<?php
/**
 * Created by PhpStorm.
 * User: markgangel
 * Date: 2018. 01. 17.
 * Time: 19:00
 */

namespace App\Repository;


use App\Entity\AttributeValue;
use App\Entity\Part;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Proxies\__CG__\App\Entity\PartCategory;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PartRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Part::class);
    }

    public function findExpiringParts()
    {
        return $this->createQueryBuilder('p')
            ->select('p.name, p.expiresAt')
            ->andWhere('p.expiresAt IS NOT NULL')
            ->getQuery()
            ->getResult();
    }

    public function getAllPartsByFilters($filters)
    {


        $query = $this->createQueryBuilder("part")
            ->join('part.attributes', 'av')
            ->join('part.partCategory', 'pc');

        foreach($filters as $filter)
        {

            $query->andWhere($filter->getTable() . ' = :filter');
            $query->setParameter('filter', $filter->getFilter());

        }


        return $query->getQuery()->getResult();

    }

    public function findByAttr($attribute)
    {
        /*
        return $this->createQueryBuilder('part')
            ->join('part.attributes', 'a')
            ->andWhere('a.id = :attribute')
            ->setParameter('attribute', $attribute->getId())
            ->getQuery()
            ->getResult();

        */


        return $this->createQueryBuilder('part')
            ->join('part.pics', 'p')
            ->join('part.attributes', 'a')
            ->setMaxResults(10)
            ->andWhere('a.id = :attribute')
            ->setParameter('attribute', $attribute->getId())
            ->getQuery()
            ->getResult();


    }

    public function findByCategory($category)
    {

        return $this->createQueryBuilder('part')
            ->join('part.pics', 'p')
            ->join('part.partCategory', 'pc')
            ->setMaxResults(10)
            ->andWhere('pc.id = :category')
            ->setParameter('category', $category->getId())
            ->getQuery()
            ->getResult();


    }


}