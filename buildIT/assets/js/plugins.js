
var $ = require('jquery');

// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');
//require('popper.js');
require('sweetalert');

require('startbootstrap-sb-admin/js/sb-admin');

