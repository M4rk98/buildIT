(function(window, $, Routing){

    'use strict';

    window.EditorApp = function ($wrapper){
        this.$wrapper = $wrapper;
        this.filter = new Filter(this.$wrapper);
        this.partHandler = new PartHandler(this.$wrapper);

        this.$wrapper.on(
            'click',
            '.navbar-sidenav .nav-link',
            this.listSubCategory.bind(this)
        );

        this.$wrapper.on(
            'click',
            '.js-add-part',
            this.partHandler.addPartToConfiguration.bind(this.partHandler)
        );

        this.$wrapper.on(
            'click',
            '.js-show-part',
            this.partHandler.showSpecificPart.bind(this.partHandler)
        );

        this.$wrapper.on(
            'click',
            '.js-delete-part',
            this.partHandler.deleteFromCurrentPartList.bind(this.partHandler)
        );


        this.$wrapper.on(
            'click',
            '.js-clear-conf-parts',
            this.partHandler.clearCurrentConfig.bind(this.partHandler)
        );

        this.$wrapper.on(
            'click',
            '.js-save-conf',
            this.partHandler.saveCurrentConfig.bind(this.partHandler)
        );

        //console.log(this.helper, Object.keys(this.helper));
        //console.log(Helper, Object.keys(Helper));


    };

    $.extend(window.EditorApp.prototype, {


        _selectors: {
        },

        _clearForm: function(){
            this._removeFormErrors();

            var $form = this.$wrapper.find(this._selectors.newRepForm);
            $form[0].reset();
        },

        _removeFormErrors: function(){
            var $form = this.$wrapper.find(this._selectors.newRepForm);
            $form.find('.js-field-error').remove();
            $form.find('.form-group').removeClass('has-error');
        },

        _addCategoryList: function($element, data){

            var html;
            var self;

            html = "<ul>";

            $.each(data.attributeCategory, function(){

                self = this;

                html += "<li class='attributeCategory' data-id='"+ this.id +"'><a>"+ this.name +"</a>";

                html += "<ul class='attribute-menu'>";

                $.each(this.attributes, function(){
                    html += "<li class='attribute' data-id='"+ this.id +"'><a>" + this.name + "</a></li>";
                });
                html += "</ul></li>";

                /*
                $.each(data.attributes, function() {
                    if(self.attributeCategory.id == this.attributeCategory.id) {
                        if(i == 0) {
                        }
                        html += "<ul class='attribute-menu'>";
                        html += "<li data-name='"+ this.name +"'><a>" + this.name + "</a></li>";
                        html += "</ul>";
                    }
                });

                */


                // TODO: dupla each minden elem esetén újra bejárom a tömböt
                /*
                $.each(data.attrs, function(){
                    if(self.name == this.attributeType.name) {
                        html += "<ul class='attribute-menu'>";
                            html += "<li data-name='"+ this.name +"'><a>" + this.name + "</a></li>";
                        html += "</ul>";
                    }
                });
                */

            });

            html += "</ul>";

            $element.append($(html));
            $element.find("ul").slideDown("slow");

        },

        _removeCategoryList: function($element) {

            $element.find("ul").slideUp("slow", function(){
                $element.find("ul").remove();
            });
        },

        _mapErrorsToForm: function(errorData){
            var $form = this.$wrapper.find(this._selectors.newRepForm);
            this._clearForm();


            $form.find(':input').each(function(){

                //console.log('error :((');
                var fieldName = $(this).attr('name');
                var $wrapper = $(this).closest('.form-group');

                if(!errorData['errors'][fieldName])
                {
                    return;
                }

                var $error = $('<span class="js-field-error help-block"></span>');
                $error.html(errorData['errors'][fieldName]);

                $wrapper.append($error);
                $wrapper.addClass('has-error');

            });

        },

        _saveReplog: function(data){
            return new Promise(function(resolve, reject){
                $.ajax({
                    url: Routing.generate('rep_log_new'),
                    method: 'POST',
                    data: JSON.stringify(data)
                }).then(function(data, textStatus, jqXHR){
                    //console.log(jqXHR.getResponseHeader('Location'));
                    $.ajax({
                        'url': jqXHR.getResponseHeader('Location')
                    }).then(function(data){
                        resolve(data);
                    })
                }).catch(function(jqXHR){
                    var errorData = JSON.parse(jqXHR.responseText);
                    reject(errorData);
                })
            });

        },


        load: function(){
            var self = this;

            $.ajax({
                url: Routing.generate('rep_log_list')
            }).then(function(data){
                $.each(data.items, function(key, repLog){
                    self._addRow(repLog);
                })
            });
        },

        _getPartsByCategory: function(category_id) {
            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: Routing.generate('list_category', {id: category_id})
                }).then(function(data){
                    resolve(data);
                }).catch(function(jqXHR){
                    var errorData = JSON.parse(jqXHR.responseText);
                    reject(errorData);
                })
            });

        },

        listPartsByFilters: function($element) {

            var self = this;
            var filters;
            var data;

            if($element.hasClass("js-main-category")) {
                data = self._getPartsByCategory($element.data("id"));
            }
            /*
            else {
                filters = {
                    "type": "attribute",
                    "name": $element.data("name")
                }
            }


            console.log(filters);
            self.filter._filters.push(filters);
            self.filter.getObjectsFromFilter();
            */

            self.filter._updatePartTable(data.parts);

        },

        _refreshParts: function(data) {
            var html;
            var $element = this.$wrapper.find('.js-parts tbody');
            $element.empty();

            console.log(data);

            html += '<tr>';
            var i = 1;

            $.each(data, function(){

                if(i % 4 == 0) {
                    html += '</tr><tr>';
                }

                 if(this.pics.length > 1) {
                     html += '<td data-id="'+ this.id +'"><img src="img/Part/'+ this.pics +'" />' + this.name + '' +
                         '<i class="fa fa-plus-circle text-success js-add-part" aria-hidden="true"></i>' +
                         '</td>';

                 } else {
                     html += '<td data-id="'+ this.id +'"><img src="img/Part/'+ this.pics[0].name +'" />' + this.name + '' +
                         '<i class="fa fa-plus-circle text-success js-add-part" aria-hidden="true"></i>' +
                         '</td>';
                 }

                i++;
            });

            html += '</tr>';

            $element.html(html).hide().fadeIn("slow");

        },
        
        _refreshPartList: function (data) {

            var $element, self, html, $new_element, $breadcrumb, $breadcrumb_def;
            self = this;

            $element = self.$wrapper.find('.js-parts');
            var $element_table = $element.children().first();


            $element.siblings('ol').find('.js-current-category').remove();

            console.log($element.siblings('ol').find('.js-current-category'));

            $breadcrumb = $element.siblings('ol').find('li').clone();
            $breadcrumb.addClass('js-current-category');

            $breadcrumb.html(data[0].partCategory.name);

            $element.siblings('ol').append($breadcrumb);

            $element.empty();

            $.each(data, function(){
                $new_element = $element_table.clone();

                $new_element.find('.part-name').html(this.name);
                $new_element.find('.part-pic img').attr('src', '/img/Part/' + this.pics[0].name);

                $new_element.find('.part-info').find('.attr').remove();
                $.each(this.attributes, function(){
                   $new_element.find('.part-info').append('<span class="attr">'+ this.attributeCategory.name +': '+ this.name +', </span>')
                });

                $new_element.find('.js-add-part').parent().attr('data-id', this.id);

                $element.prepend($new_element);


            });


        },

        listSubCategory: function(e) {

            var self = this;
            var $element = $(e.currentTarget);

            if($element.closest('ul').hasClass('sidenav-third-level')) {
                $.ajax({
                    url: Routing.generate('list_parts_by_attribute', {id: $element.data("id")})
                }).then(function (data) {
                    //self._refreshParts(data);
                    self._refreshPartList(data);
                    console.log(data);

                });

                return false;
            }

            console.log($element.data('id'));

            $.ajax({
                url: Routing.generate('list_parts_by_category', {id: $element.data("id")})
            }).then(function (data) {
                //console.log(data);
                //self._addCategoryList($element, data);
                self._refreshPartList(data);

            });


            /*
            if($element.hasClass("attribute")) {
                $.ajax({
                    url: Routing.generate('list_parts_by_attribute', {id: $element.data("id")})
                }).then(function (data) {
                    self._refreshParts(data);

                });

                return;
            }

            if($element.children("ul").length != 0)
            {
                self._removeCategoryList($element);
            } else {
                $.ajax({
                    url: Routing.generate('list_category', {id: $element.data("id")})
                }).then(function (data) {
                    console.log(data);
                    self._addCategoryList($element, data);
                    self._refreshParts(data.parts);

                });

            }
            */

        },

        listAttributes: function()
        {
            var self = this;

            $.ajax({
                url: Routing.generate('sub_category_list', {id: $element.data("id")})
            }).then(function(data){
                self._addList($element, data);
                //self.listAttributes();
            });


        },

        handleNewFormSubmit: function(e) {

            e.preventDefault();

            var $form = $(e.currentTarget);
            var formData = {};

            $.each($form.serializeArray(), function(key, fieldData){
                formData[fieldData.name] = fieldData.value;
            });

            var self = this;
            self._saveReplog(formData).then(function(data) {
                self._addRow(data)
            }).catch(function(errorData){
                self._mapErrorsToForm(errorData);
            })
            ;


        },

        updateTotalWeightsLifted: function(){

            this.$wrapper.find('.js-total-weight').html(this.helper.calculateTotalWeight());

        },

        _replogDelete: function($link) {
            $link.addClass('text-danger');

            $link.find('.fa')
                .removeClass('fa-trash')
                .addClass('fa-spinner')
                .addClass('fa-spin');

            var deleteUrl = $link.data('url');
            var $rowToDelete = $link.closest('tr');
            var self = this;

            return $.ajax({
                url: deleteUrl,
                method: 'DELETE'
            }).then(function(){
                $rowToDelete.fadeOut('normal', function(){
                    $(this).remove();
                    self.updateTotalWeightsLifted();
                });
            });
        },

        handleRepLogDelete: function(e){

            e.preventDefault();
            var $link = $(e.currentTarget);
            var self = this;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    return self._replogDelete($link);
                }
            }).then(function () {
                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
            }).catch(function(){
                console.log('cancelled');
            })


        },

        handleRowClick: function(){
            console.log('row clicked');
        }

    });

    /**
     * private object
     */
    var Filter =  function($wrapper) {
        this.$wrapper = $wrapper;
    };

    $.extend(Filter.prototype, {

        _filters: [],

        _updatePartTable: function(data) {
            //TODO: list.html.twig -> js-partsban clearelni és hozzáadni

            var html;
            var $element = this.$wrapper.find('.js-parts tbody');
            $element.empty();

            html += '<tr>';
            var i = 1;

            $.each(data, function(){
                if(i % 4 == 0) {
                    html += '</tr><tr>';
                }
                html += '<td><img src="img/Part/'+ this.pictures[0].name +'" />' + this.name + '</td>';
                i++;
            });

            html += '</tr>';

            $element.html(html).hide().fadeIn("slow");

        },

        getObjectsFromFilter: function() {

            var self = this;

            $.ajax({
                url: Routing.generate('part_list'),
                method: 'POST',
                data: JSON.stringify(this._filters)
            }).then(function (data) {
                console.log(data);
                //console.log(self.$wrapper.find(".js-parts"));
                // TODO: list.html.twig
                //self._addCategoryList($element, data.attributes);

                self._updatePartTable(data);

            });
        }

    });

    var PartHandler = function($wrapper) {
        this.$wrapper = $wrapper;

        this.__construct();
    };

    $.extend(PartHandler.prototype, {

        _currentParts: [],


        __construct: function()
        {
            var self = this;

            if(self._currentParts.length == 0 && self.$wrapper.find('.js-current-parts').children().length != 0)
            {
                $.each(self.$wrapper.find('.js-current-parts').children(), function(){
                    self._getPart($(this).find('.data').data('id')).then(function(data){
                        self._currentParts.push(data);
                    });
                });

            }
        },

        _getPart: function(part_id) {
            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: Routing.generate('part_show_js', {id: part_id})
                }).then(function (data) {
                    resolve(data);
                }).catch(function(jqXHR){
                    var errorData = JSON.parse(jqXHR.responseText);
                    reject(errorData);
                });
            });
        },


        _fillPartPage: function(data) {

            console.log(data);

            var div = document.createElement("div");
            div.className = "js-conf-part-img";
            var ul = document.createElement("ul");
            ul.className = "part-attributes";
            var li;
            var img = document.createElement("img");
            img.src = '/img/Part/' + data.pics[0].name;

            var hr = document.createElement("hr");

            $.each(data.attributes, function() {
                li = document.createElement("li");
                li.innerHTML = '<b>' + this.attributeCategory.name +'</b>: '+ this.name;
                ul.appendChild(li);
            });

            div.appendChild(img);
            div.appendChild(hr);
            div.appendChild(ul);

            swal({
                title: data.name,
                content: div,
                button: {
                    text: "Bezár",
                    value: true,
                    visible: true,
                    className: "bg-danger",
                    closeModal: true
                }
            });

        },

        deleteFromCurrentPartList: function(e) {
            var self, $part_element;

            self = this;

            console.log(self._currentParts);

            $part_element = $(e.currentTarget);

            var part_id = $part_element.closest("div").data('id');
            console.log(part_id);

            var data = $.grep(self._currentParts, function(e){
                return e.id != part_id;
            });

            self._currentParts = data;
            self._currentParts.shift();

            self.refreshCurrentParts();

            swal("Alkatrészt sikeresen kivetted.");

        },

        refreshCurrentParts: function() {

            var $part_element, self, html;

            self = this;
            $part_element = self.$wrapper.find('.js-current-parts');

            $part_element.empty();

            $.each(self._currentParts, function(){
                html =
                '<div class="col-xl-3 col-sm-6 mb-3">' +
                    '<div class="card o-hidden h-100">' +
                    '<div class="card-body">' +
                    '<div class="card-body-icon">' +
                    '<img src="/img/Part/'+ this.pics[0].name +'" width="100" alt="">'+
                    '</div>'+
                    '<div class="mr-5">'+ this.name +'</div>'+
                '</div>'+
                '<div class="card-footer clearfix small z-1" data-id="'+ this.id +'">'+
                    '<span class="float-left"><a href="#" class="js-show-part">Részletek</a></span>'+
                    '<span class="float-right">'+
                    '<a href="#" class="js-delete-part text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>'+
                    '</span>'+
                '</div>'+
                    '</div>'+
                    '</div>';

                $part_element.append(html);

            });


        },

        saveCurrentConfig: function() {

            var self = this;

            if(self._currentParts.length == 0 && self.$wrapper.find('.js-current-parts').children().length == 0)
            {
                swal("Hoppá!",  "Elfelejtettél alkatrészt beszerelni! Próbáld újra!", "error");
                return false;
            }

            swal("Nevezd el a munkádat!", {
                content: "input"
            }).then(function(value){

                self._currentParts.unshift({"name": value});

                $.ajax({
                    url: Routing.generate('new_config'),
                    method: 'POST',
                    data: JSON.stringify(self._currentParts)
                }).then(function(){
                    swal("Szép munka!", "Konfiguráció mentve!", "success").then(function(){
                        window.location.href = Routing.generate("user_config");
                    });
                }).catch(function(data){
                    swal("Hoppá!",  "A foglalatok nem egyeznek meg az alkatrészek között!", "error");
                });


            });


        },

        showSpecificPart: function(e) {
            var self = this;
            var html;
            var $element = $(e.currentTarget).closest('div');

            this._getPart($element.data('id')).then(function(data){
                self._fillPartPage(data);
            });


        },


        addPartToConfiguration: function(e) {

            var $element = $(e.currentTarget);
            var self = this;

            console.log($element.parent());

            var element_id = $element.parent().data('id');

            self._getPart(element_id).then(function(data){
                self._currentParts.push(data);
                self.refreshCurrentParts();
            });

            swal("Szép munka!", "Alkatrész hozzáadva!", "success");
        },

        clearCurrentConfig: function() {

            var self = this;

            swal({
                title: "Biztos törlöd a konfigurációt?",
                text: "Ha törlöd újra kell kezdened az egész folyamatot!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function(willDelete){
                if (willDelete) {
                    self._currentParts = [];
                    self.refreshCurrentParts();
                    swal("Poof! Az eddigi munkádat töröltük!", {
                        icon: "success",
                    });
                } else {
                    swal("Munkád biztonságban megmaradt!");
                }
            });

        }

    })



})(window, $, Routing);